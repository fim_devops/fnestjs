#!/bin/bash

echo { >> ormconfig.json
echo \"username\": \"$DB_USERNAME\", >> ormconfig.json
echo \"password\": \"$DB_PASSWORD\", >> ormconfig.json
echo \"database\": \"$DB_NAME\", >> ormconfig.json
echo \"type\": \"postgres\", >> ormconfig.json
echo \"host\": \"db\", >> ormconfig.json
echo \"port\": 5432, >> ormconfig.json
echo \"synchronize\": true, >> ormconfig.json
echo \"logging\": false, >> ormconfig.json
echo \"entities\": [\"dist/**/*.entity.js\"], >> ormconfig.json
echo \"migrations\": [\"src/migration/**/*.ts\"] >> ormconfig.json
echo } >> ormconfig.json