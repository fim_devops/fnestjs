FROM node:16-alpine as development 
#RUN groupadd -r node && useradd -g node node 
WORKDIR /home/node 
#update npm
#RUN npm install -g npm 
RUN chown -R node:node /home/node 
USER node 
COPY package.json package-lock.json ./ 
RUN npm ci --development 
COPY . /home/node 
RUN npm run build 

FROM node:16-alpine as production 
WORKDIR /home/node 
RUN apk add --no-cache bash
#update npm
#RUN npm install -g npm 
RUN chown -R node:node /home/node 
COPY package.json package-lock.json ormconfig.json ./ 
RUN npm ci --production
COPY --from=development /home/node/dist ./dist
EXPOSE 3000/tcp
CMD ["node", "dist/main"]

