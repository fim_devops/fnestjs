import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ParseIntPipe } from '@nestjs/common/pipes/parse-int.pipe';
import { Customer } from './customer.entity';
import { CustomersService } from './customers.service';

@Controller('customers')
export class CustomersController {
  constructor(private customersService: CustomersService) {}

  @Get()
  findAll() {
    return this.customersService.getCustomers();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id) {
    return this.customersService.findOne(id);
  }

  @Post() create(@Body() customer: Customer) {
    return this.customersService.createCustomer(customer);
  }

  @Patch(':id')
  async editNote(@Body() customer: Customer, @Param('id') id: number): Promise<Customer> {
    const noteEdited = await this.customersService.editCustomer(id, customer);
    return noteEdited;
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id) {
    this.customersService.remove(id);
  }
}