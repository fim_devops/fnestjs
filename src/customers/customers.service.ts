import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from './customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer) private customersRepository: Repository<Customer>,
  ) {}
  async getCustomers(): Promise<Customer[]> {
    return await this.customersRepository.find();
  }

  findOne(id: string): Promise<Customer> {
    return this.customersRepository.findOne(id);
  }

  async createCustomer(customer: Customer) {
    this.customersRepository.save(customer);
  }

  async remove(id: string): Promise<void> {
    await this.customersRepository.delete(id);
  }

  async editCustomer(id: number, customer: Customer): Promise<Customer> {
    const editedCustomer = await this.customersRepository.findOne(id);
    if (!editedCustomer) {
      throw new NotFoundException('Customer is not found');
    }
    editedCustomer.name = customer.name;
    editedCustomer.description = customer.description;
    await editedCustomer.save();
    return editedCustomer;
  }
}