import { BaseEntity, Entity, Column, PrimaryGeneratedColumn} from "typeorm"

@Entity()
export class Customer extends BaseEntity{
    @PrimaryGeneratedColumn()
	id: number;

    @Column()
    name: string;

    @Column("text")
    description: string;
}